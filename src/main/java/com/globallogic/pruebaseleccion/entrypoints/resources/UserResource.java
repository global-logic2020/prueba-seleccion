package com.globallogic.pruebaseleccion.entrypoints.resources;

import com.globallogic.pruebaseleccion.core.domain.entities.User;
import com.globallogic.pruebaseleccion.core.useCases.exceptions.EmailAlreadyExistsException;
import com.globallogic.pruebaseleccion.core.useCases.exceptions.EmailFormatException;
import com.globallogic.pruebaseleccion.core.useCases.exceptions.PasswordFormatException;
import com.globallogic.pruebaseleccion.entrypoints.EnpointResponse;
import com.globallogic.pruebaseleccion.entrypoints.facades.UserFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;

@RestController
@RequestMapping("/user")
public class UserResource {

    private static final Logger LOG = LoggerFactory.getLogger(UserResource.class);

    private final UserFacade userFacade;

    public UserResource(UserFacade userFacade){

        LOG.info("CREACION INSTACIA CLASE USER RESOURCE");

        this.userFacade = userFacade;
    }

    @PostMapping
    public ResponseEntity<EnpointResponse<User>> createUser(@RequestBody User userRequest) throws URISyntaxException {

        LOG.info("INICIO ENDPOINT CREATE USER");

        EnpointResponse<User> response = new EnpointResponse<>();

        try{
            response.setData(this.userFacade.createUser(userRequest));
        }catch (EmailAlreadyExistsException | EmailFormatException | PasswordFormatException e){
            LOG.info("EXISTE ERROR DE VALIDACION");

            response.setMensaje(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }

        return ResponseEntity.status(HttpStatus.OK)
                .body(response);

    }
}
