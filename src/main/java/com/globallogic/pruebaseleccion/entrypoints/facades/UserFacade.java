package com.globallogic.pruebaseleccion.entrypoints.facades;

import com.globallogic.pruebaseleccion.core.domain.entities.User;
import com.globallogic.pruebaseleccion.core.useCases.CreateUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class UserFacade {

    private static final Logger LOG = LoggerFactory.getLogger(UserFacade.class);

    private final CreateUser createUser;

    public UserFacade(CreateUser createUser) {

        LOG.info("CREACION INSTACIA CLASE USER FACADE");

        this.createUser = createUser;
    }

    public User createUser(User user) {

        LOG.info("INICIO METODO CREATE USER EN CLASE USER FACADE");

        return createUser.create(user);
    }

}
