package com.globallogic.pruebaseleccion.core.useCases.exceptions;

public class EmailFormatException extends RuntimeException {
    public EmailFormatException(final String message) {
        super(message);
    }
}
