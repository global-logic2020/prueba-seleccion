package com.globallogic.pruebaseleccion.core.useCases.ports;

import com.globallogic.pruebaseleccion.core.domain.entities.User;

import java.util.Optional;

public interface UserRepository {

    User create(User user);

    Optional<User> findByEmail(String email);

}
