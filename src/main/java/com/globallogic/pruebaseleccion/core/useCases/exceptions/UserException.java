package com.globallogic.pruebaseleccion.core.useCases.exceptions;

public class UserException extends RuntimeException {
    public UserException(final String message) {
        super(message);
    }
}
