package com.globallogic.pruebaseleccion.core.useCases.exceptions;

public class EmailAlreadyExistsException  extends RuntimeException {
    public EmailAlreadyExistsException(final String message) {
        super(message);
    }
}
