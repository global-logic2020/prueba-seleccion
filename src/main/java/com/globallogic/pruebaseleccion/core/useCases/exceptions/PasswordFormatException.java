package com.globallogic.pruebaseleccion.core.useCases.exceptions;

public class PasswordFormatException extends RuntimeException {
    public PasswordFormatException(final String message) {
        super(message);
    }
}
