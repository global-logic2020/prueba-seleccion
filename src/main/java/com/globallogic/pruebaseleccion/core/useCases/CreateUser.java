package com.globallogic.pruebaseleccion.core.useCases;

import com.globallogic.pruebaseleccion.core.domain.entities.User;
import com.globallogic.pruebaseleccion.core.domain.model.UserBuilder;
import com.globallogic.pruebaseleccion.core.domain.util.UuidGenerator;
import com.globallogic.pruebaseleccion.core.useCases.exceptions.EmailAlreadyExistsException;
import com.globallogic.pruebaseleccion.core.useCases.ports.UserRepository;
import com.globallogic.pruebaseleccion.core.useCases.validators.EmailValidator;
import com.globallogic.pruebaseleccion.core.useCases.validators.PasswordValidator;
import com.globallogic.pruebaseleccion.core.useCases.validators.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.UUID;

public class CreateUser {

    private static final Logger LOG = LoggerFactory.getLogger(CreateUser.class);

    private final UserRepository userRepository;
    //private final UuidGenerator idGenerator = new UuidGenerator();

    public CreateUser(UserRepository userRepository) {
        LOG.info("INICIAR CASO DE USO CREATE USER");
        this.userRepository = userRepository;
    }

    public User create(final User user) {

        LOG.info("INICIO EJECUCION FUNCION CREATE EN CASO DE USO CREATE USER");

        UserValidator.validateCreateUser(user);

        EmailValidator.validateFormat(user.getEmail());
        PasswordValidator.validateFormat(user.getPassword());

        if (this.userRepository.findByEmail(user.getEmail()).isPresent()) {
            LOG.info("VALIDACION DE CORREO EXISTENTE");
            throw new EmailAlreadyExistsException("El correo "+user.getEmail()+" ya esta registrado");
        }

        /*User newUser = new UserBuilder()
                .email(user.getEmail())
                .password(user.getPassword())
                .name(user.getName())
                .build();*/

        user.setCreated(LocalDateTime.now());
        user.setLastLogin(LocalDateTime.now());
        user.setActive(Boolean.TRUE);
        user.setToken(UUID.randomUUID().toString());

        return this.userRepository.create(user);
    }

}
