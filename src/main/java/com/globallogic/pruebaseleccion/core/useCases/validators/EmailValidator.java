package com.globallogic.pruebaseleccion.core.useCases.validators;

import com.globallogic.pruebaseleccion.core.useCases.exceptions.EmailFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {

    private static final Logger LOG = LoggerFactory.getLogger(EmailValidator.class);

    private static final Pattern VALID_EMAIL_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
                                                                        Pattern.CASE_INSENSITIVE);

    public static void validateFormat(final String email){

        LOG.info("INICIO EJECUCION FUNCION VALIDATE FORMAT DE LA CLASE EMAIL VALIDATOR");

        Matcher matcher = VALID_EMAIL_REGEX .matcher(email);

        if(!matcher.find()){
            throw new EmailFormatException("El email tiene un formato incorrecto");
        }

    }
}
