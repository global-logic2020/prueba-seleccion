package com.globallogic.pruebaseleccion.core.useCases.validators;

import com.globallogic.pruebaseleccion.core.useCases.exceptions.PasswordFormatException;
import com.globallogic.pruebaseleccion.core.useCases.exceptions.UserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator {

    private static final Logger LOG = LoggerFactory.getLogger(PasswordValidator.class);

    private static final Pattern VALID_PASSWORD_REGEX = Pattern.compile("^(?=(?:.*\\d){2})(?=(?:.*[A-Z]){1})(?=(?:.*[a-z]){2})\\S{8,}$");

    public static void validateFormat(final String password){

        LOG.info("INICIO EJECUCION FUNCION VALIDATE FORMAT DE LA CLASE PASSWORD VALIDATOR");

        Matcher matcher = VALID_PASSWORD_REGEX .matcher(password);

        if(!matcher.find()){
            throw new PasswordFormatException("La clave tiene un formato incorrecto");
        }

    }
}
