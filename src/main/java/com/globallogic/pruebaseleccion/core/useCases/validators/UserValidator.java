package com.globallogic.pruebaseleccion.core.useCases.validators;

import com.globallogic.pruebaseleccion.core.domain.entities.User;
import com.globallogic.pruebaseleccion.core.useCases.exceptions.UserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserValidator {

    private static final Logger LOG = LoggerFactory.getLogger(UserValidator.class);

    public static void validateCreateUser(final User user) {

        LOG.info("INICIO EJECUCION FUNCION VALIDATE CREATE USER");

        if (user == null) throw new UserException("Usuario nulo");
        if (user.getEmail() == null || user.getEmail().isEmpty()) throw new UserException("El usuario debe tener un email");
        if (user.getName() == null || user.getName().isEmpty()) throw new UserException("El usuario debe tener nombre");
        if (user.getPassword() == null || user.getPassword().isEmpty()) throw new UserException("El usuario debe tener clave");
    }
}
