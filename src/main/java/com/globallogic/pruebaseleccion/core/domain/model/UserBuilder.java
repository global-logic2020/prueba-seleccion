package com.globallogic.pruebaseleccion.core.domain.model;

import com.globallogic.pruebaseleccion.core.domain.entities.Phone;
import com.globallogic.pruebaseleccion.core.domain.entities.User;

import java.util.UUID;

public class UserBuilder {

    private final User user;

    public UserBuilder(){
        this.user = new User();
    }

    public UserBuilder id(final Long id) {
        this.user.setId(id);
        return this;
    }

    public UserBuilder name(final String name) {
        this.user.setName(name);
        return this;
    }

    public UserBuilder email(final String email) {
        this.user.setEmail(email);
        return this;
    }

    public UserBuilder password(final String password) {
        this.user.setPassword(password);
        return this;
    }

    public UserBuilder addPhone(final Phone phone){
        this.user.getPhones().add(phone);
        return this;
    }

    public User build() {
        return user;
    }
}
