package com.globallogic.pruebaseleccion.core.domain.util;

import org.springframework.util.IdGenerator;

import java.util.Random;
import java.util.UUID;

public class UuidGenerator implements IdGenerator {

    @Override
    public UUID generateId() {
        return UUID.randomUUID();
    }
}
