package com.globallogic.pruebaseleccion.core.domain.entities;

import java.util.Objects;

public class Phone {
    private Long number;
    private Integer citycode;
    private Integer contrycode;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Integer getCitycode() {
        return citycode;
    }

    public void setCitycode(Integer citycode) {
        this.citycode = citycode;
    }

    public Integer getContrycode() {
        return contrycode;
    }

    public void setContrycode(Integer contrycode) {
        this.contrycode = contrycode;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone = (Phone) o;
        return Objects.equals(number, phone.number) &&
                Objects.equals(citycode, phone.citycode) &&
                Objects.equals(contrycode, phone.contrycode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, citycode, contrycode);
    }

    @Override
    public String toString() {
        return "Phone{" +
                "number=" + number +
                ", citycode=" + citycode +
                ", contrycode=" + contrycode +
                '}';
    }
}
