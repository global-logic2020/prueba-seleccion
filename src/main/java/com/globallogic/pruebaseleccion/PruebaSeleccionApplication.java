package com.globallogic.pruebaseleccion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaSeleccionApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaSeleccionApplication.class, args);
	}

}
