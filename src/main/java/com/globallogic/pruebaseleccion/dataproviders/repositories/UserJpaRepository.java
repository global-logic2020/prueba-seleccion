package com.globallogic.pruebaseleccion.dataproviders.repositories;

import com.globallogic.pruebaseleccion.dataproviders.data.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserJpaRepository extends JpaRepository<UserEntity, UUID> {

    Optional<UserEntity> findByEmail(String email);
}
