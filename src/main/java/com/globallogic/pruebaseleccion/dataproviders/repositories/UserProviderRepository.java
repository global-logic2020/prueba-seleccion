package com.globallogic.pruebaseleccion.dataproviders.repositories;

import com.globallogic.pruebaseleccion.core.domain.entities.User;
import com.globallogic.pruebaseleccion.core.useCases.ports.UserRepository;
import com.globallogic.pruebaseleccion.dataproviders.data.UserEntity;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class UserProviderRepository implements UserRepository {

    private static final Logger LOG = LoggerFactory.getLogger(UserProviderRepository.class);

    private final UserJpaRepository userJpaRepository;
    private final ModelMapper modelMapper;

    public UserProviderRepository(UserJpaRepository userJpaRepository,
                                  ModelMapper modelMapper) {

        LOG.info("CREACION CLASE USER PROVIDER REPOSITORY");

        this.userJpaRepository = userJpaRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public User create(User user) {

        LOG.info("INCIO METODO CREATE EN CLASE USER PROVIDER REPOSITORY");

        UserEntity userEntity = modelMapper.map(user, UserEntity.class);
        UserEntity userEntitySaved =  this.userJpaRepository.save(userEntity);

        return modelMapper.map(userEntitySaved, User.class);
    }

    @Override
    public Optional<User> findByEmail(String email) {

        LOG.info("INCIO METODO BUSCAR POR EMAIL EN CLASE USER PROVIDER REPOSITORY");

        Optional<UserEntity> userEntityFound = this.userJpaRepository.findByEmail(email);

        if(userEntityFound.isPresent()){
            User user = modelMapper.map(userEntityFound.get(), User.class);
            return Optional.of(user);
        }

        return Optional.empty();
    }
}
