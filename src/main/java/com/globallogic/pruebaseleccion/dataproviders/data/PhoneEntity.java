package com.globallogic.pruebaseleccion.dataproviders.data;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "phone_user")
public class PhoneEntity {
    @Id
    @GeneratedValue
    private Long id;
    private Long number;
    private Integer citycode;
    private Integer contrycode;
}
