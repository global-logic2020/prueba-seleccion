package com.globallogic.pruebaseleccion.configuration;

import com.globallogic.pruebaseleccion.core.useCases.CreateUser;
import com.globallogic.pruebaseleccion.core.useCases.ports.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {

    @Bean
    public CreateUser createUser(UserRepository userRepository) {
        return new CreateUser(userRepository);
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}
