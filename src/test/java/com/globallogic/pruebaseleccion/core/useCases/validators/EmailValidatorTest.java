package com.globallogic.pruebaseleccion.core.useCases.validators;

import com.globallogic.pruebaseleccion.core.useCases.exceptions.EmailFormatException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class EmailValidatorTest {

    @Test
    public void validateFormatSucceeds(){
        String emailTesting = "formato.correcto@email.com";

        EmailValidator.validateFormat(emailTesting);
    }

    @Test
    public void validateFormatWithEmailFormatException(){

        String emailTesting = "formato.incorrecto.email.com";

        Exception exception = assertThrows(EmailFormatException.class, () -> {
            EmailValidator.validateFormat(emailTesting);
        });

        String expectedMessage = "El email tiene un formato incorrecto";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

}
