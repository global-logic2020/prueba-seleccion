package com.globallogic.pruebaseleccion.core.useCases.validators;

import com.globallogic.pruebaseleccion.core.useCases.exceptions.PasswordFormatException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PasswordValidatorTest {

    @Test
    public void validateFormatSucceeds(){
        String passwordTesting = "alguNaclave12";

        PasswordValidator.validateFormat(passwordTesting);
    }

    @Test
    public void validateFormatWithPasswordFormatException(){

        String passwordTesting = "algunaclave12";

        Exception exception = assertThrows(PasswordFormatException.class, () -> {
            PasswordValidator.validateFormat(passwordTesting);
        });

        String expectedMessage = "La clave tiene un formato incorrecto";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
}
