###### **Evaluación JAVA.**

Este proyecto está construido en Java mediante Spring Boot. La arquitectura de la aplicación está basada en "Arquitectura Limpia"


Para levantar la aplicación se debe ejecutar el comando

**gradlew bootRun**

para ejecutar el endpoint que permite crear un usuario se debe acceder a la siguiente URL

[POST] **localhost:8080/user** 


Para visualizar la base de datos en memoria se debe ingresar a la siguiente URL http://localhost:8080/h2-console

**nombre base de datos** = usuario

**url de conexión** = jdbc:h2:mem:usuario

**usuario** = sa

**password** =   (en blanco)


Diagrama de secuencia ubicado en la ruta (src/main/resources/static/CreateUserEndpoint.png)

![Diagrama de secuencia]("src/main/resources/static/CreateUserEndpoint.png")


Diagrama de componentes ubicado en la ruta (src/main/resources/static/ComponentDiagram.png)

![Diagrama de secuencia]("src/main/resources/static/ComponentDiagram.png")
